import mysql.connector
class sql_crudoperations:

	def create_database():
		connection = mysql.connector.connect(
		host="localhost",
		database="sasi1",
		user="root",
		password="Innominds"
		)
		mycursor = connection.cursor()
		mycursor.execute("CREATE DATABASE rushmitha")
		print("the database is created")

	def create_table():
		connection = mysql.connector.connect(
		host="localhost",
		database="sasi1",
		user="root",
		password="Innominds"
		)
		mycursor = connection.cursor()
		mycursor.execute("create table employees(ename varchar(255),salary varchar(255))")
		print("the table is created")

	def create_record():
		connection = mysql.connector.connect(
		host="localhost",
		database="sasi1",
		user="root",
		password="Innominds"
		)
		mycursor = connection.cursor()
		insert_command = "INSERT INTO employees (ename, salary) VALUES (%s, %s)"
		values = [
			('sasi', '100000'),
			('nani', '900000'),
			('saru', '800000'),
			('rushmitha','850000'),
			('arshith','100000'),
			('raju','60000')
		]

		mycursor.executemany(insert_command, values)

		connection.commit()

		print(mycursor.rowcount, "are inserted.")
		print("the records are created")

	def read_database():
		connection = mysql.connector.connect(
		host="localhost",
		database="sasi1",
		user="root",
		password="Innominds"
		)
		mycursor = connection.cursor()
		mycursor.execute("SHOW DATABASES")
		for i in mycursor:
			print(i)
	def display_tables():
		connection = mysql.connector.connect(
		host="localhost",
		database="sasi1",
		user="root",
		password="Innominds"
		)
		mycursor = connection.cursor()
		mycursor.execute("SHOW tables")
		for i in mycursor:
			print(i)
	def read_table():
		connection = mysql.connector.connect(
		host="localhost",
		database="sasi1",
		user="root",
		password="Innominds"
		)
		mycursor = connection.cursor()
		mycursor.execute("select * from employees")
		for i in mycursor:
			print(i)
	def read_tables():
		connection = mysql.connector.connect(
		host="localhost",
		database="sasi1",
		user="root",
		password="Innominds"
		)
		mycursor = connection.cursor()
		mycursor.execute("select ename from employees where salary>=100000")
		for i in mycursor:
			print(i)
	def rename_table():
		connection = mysql.connector.connect(
		host="localhost",
		database="sasi1",
		user="root",
		password="Innominds"
		)
		mycursor = connection.cursor()
		mycursor.execute("alter table employees rename to employees1")
		print("the name of the table is changed")
	def add_column_table():
		connection = mysql.connector.connect(
		host="localhost",
		database="sasi1",
		user="root",
		password="Innominds"
		)
		mycursor = connection.cursor()
		mycursor.execute("alter table employees1 add email varchar(255)")
		print("the new column is added to the table")
	def drop_column_table():
		connection = mysql.connector.connect(
		host="localhost",
		database="sasi1",
		user="root",
		password="Innominds"
		)
		mycursor = connection.cursor()
		mycursor.execute("alter table employees1 drop column email")
		print("the column of the table is deleted")
	def update_record():
		connection = mysql.connector.connect(
		host="localhost",
		database="sasi1",
		user="root",
		password="Innominds"
		)
		mycursor = connection.cursor()
		mycursor.execute("update employees set ename='raj',salary='50000' where ename='raju'")
		print("the record is updated")
	def delete_record():
		connection = mysql.connector.connect(
		host="localhost",
		database="sasi1",
		user="root",
		password="Innominds"
		)
		mycursor = connection.cursor()
		mycursor.execute("delete from employees1 where ename='saru' and salary='80000'")
		print("the record is deleted ")
	def delete_table():
		connection = mysql.connector.connect(
		host="localhost",
		database="sasi1",
		user="root",
		password="Innominds"
		)
		mycursor = connection.cursor()
		mycursor.execute("drop table employees1")
		print("the table is deleted")


	def delete_database():
		connection = mysql.connector.connect(
		host="localhost",
		database="sasi1",
		user="root",
		password="Innominds"
		)
		mycursor = connection.cursor()
		mycursor.execute("delete database rushmitha")
		print("the database is deleted")
	def select_choice(self):
		print("EMPLOYEES INFORMATION")
		print("1-create a new database")
		print("2-create a new table")
		print("3-create a new record")
		print("4-reads the database")
		print("5-displays the tables")
		print("6-reads the table")
		print("7-reads the table with specified conditions")
		print("8-changes the name of the table")
		print("9-adds the new column to the table")
		print("10-deletes the column from the table")
		print("11-updates a row  in the table")
		print("12-deletes a row  in the table")
		print("13-deletes a table")
		print("14-deletes a database")
		choice=input("enter your choice:")
		while choice <"15":
			if choice =="1":
				self.create_database()
				break
			if choice=="2":
				self.create_table()
				break
			if choice=="3":
				self.create_record()
				break 
			if choice=="4":
				self.read_database()
				break
			if choice=="5":
				self.display_tables()
				break
			if choice=="6":
				self.read_table()
				break
			if choice=="7":
				self.read_tables()
				break
			if choice=="8":
				self.rename_table()
				break
			if choice=="9":
				self.add_column_table()
				break
			if choice=="10":
				self.drop_column_table()
				break
			if choice=="11":
				self.update_record()
				break
			if choice=="12":
				self.delete_record()
				break
			if choice=="13":
				self.delete_table()
				break
			if choice=="14":
				self.delete_database()
				break











