-- creating a database
create database sasi1;
-- using the created database
use sasi1;
-- creating a table
create table sathi(
firstname varchar(255),
lastname varchar(255),
age int,
gender varchar(255),
address varchar(255)
);
-- inserting the values into the table
insert into sathi(firstname,lastname,age,gender, address) 
values("sasi","sathi",23,"female","dwarapudi");
insert into sathi(firstname,lastname,age,gender,address)
values("nani","annamdevula",19,"male","palem");
insert into sathi(firstname,lastname ,age,gender,address)
values("rushmitha","sathi",20,"female","dwarapudi");
-- display the databases created
show databases;
-- describes the contents of the table
describe sathi;
-- displays all the tables created
show tables;
-- updates the record
update sathi set firstname="arshith",age=2,gender="male" where firstname="sasi";
-- changing the name of the table
alter table sathi rename to sathi1;
-- adding a new column to the table
alter table sathi1 add email varchar(255);
alter table sathi1 drop column email;
alter table sathi1 modify age varchar(255);
-- drop database sasi1
-- drop table sathi1
select * from sathi1;